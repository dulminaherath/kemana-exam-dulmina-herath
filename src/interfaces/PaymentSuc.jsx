import React from "react";
import Message from "../maincomponents/Message";
import { Link } from "react-router-dom";

const PaymentSuc = () => {
  return (
    <div>
      <Message>
        Your Payment Success !!! <Link to="/">Go Product List</Link>
      </Message>
    </div>
  );
};

export default PaymentSuc;
