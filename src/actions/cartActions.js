import { CART_ADD_ITEM, CART_REMOVE_ITEM,CART_ALLREMOVE_ITEM } from "../constants/cartConstants";
import products from "../productsbackend/products";

//Get Cart Data-Comment By Dulmina Herath
export const addToCart = (id, qty) => async (dispatch, getState) => {
  const data = await products.find((x) => x._id === id);

  dispatch({
    type: CART_ADD_ITEM,
    payload: {
      product: data._id,
      name: data.name,
      image: data.image,
      price: data.price,
      countInStock: data.countInStock,
      qty,
    },
  });

  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
};


//Remove Single Item-Comment By Dulmina Herath
export const removeFromCart = (id) =>  (dispatch, getState) => {

dispatch({
    type: CART_REMOVE_ITEM,
    payload: id,
  });

  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
};


//Remove Multiple  Items-Comment By Dulmina Herath
export const removeAllFromCart = () =>  (dispatch, getState) => {
 
 dispatch({
     type: CART_ALLREMOVE_ITEM,
     payload:{},
   });
 
   localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
 };
