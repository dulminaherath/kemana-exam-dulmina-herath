import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  Row,
  Col,
  ListGroup,
  Image,
  Button,
  Card,
  FormControl,
} from "react-bootstrap";
import Message from "../maincomponents/Message";
import {
  addToCart,
  removeFromCart,
  removeAllFromCart,
} from "../actions/cartActions";

const CartUi = ({ match, location, history }) => {
  const productId = match.params.id;
  const qty = location.search ? Number(location.search.split("=")[1]) : 1;
  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;

  useEffect(() => {
    if (productId) {
      dispatch(addToCart(productId, qty));
    }
  }, [dispatch, productId, qty]);

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id));
  };
  const checkoutHandler = () => {
    dispatch(removeAllFromCart());
  };
  const removeAllFromCartHandler = () => {
    dispatch(removeAllFromCart());
  };

  return (
    <React.Fragment>
      <ListGroup variant="flush">
        <ListGroup.Item>
          <Row>
            <Col md={5}>
              <h1>Shopping Cart</h1>
            </Col>
            <Col md={3}>
              <Button
                type="button"
                variant="light"
                hidden={cartItems.length === 0}
                onClick={() => removeAllFromCartHandler()}
              >
                {" "}
                Clear All <i className="fas fa-trash"></i>
              </Button>
            </Col>
          </Row>
        </ListGroup.Item>
        <ListGroup.Item>
          <Row>
            <Col md={8}>
              {cartItems.length === 0 ? (
                <Message>
                  Empty cart ! <Link to="/">Go Product List</Link>
                </Message>
              ) : (
                <ListGroup variant="flush">
                  {cartItems.map((item) => (
                    <ListGroup.Item key={item.product}>
                      <Row>
                        <Col md={2}>
                          <Image
                            src={item.image}
                            alt={item.name}
                            fluid
                            rounded
                          />
                        </Col>
                        <Col md={3}>
                          <Link to={`/product/${item.product}`}>
                            {item.name}
                          </Link>
                        </Col>
                        <Col md={2}>${item.price}</Col>
                        <Col md={2}>
                          <FormControl
                            as="select"
                            value={item.qty}
                            onChange={(e) =>
                              dispatch(
                                addToCart(item.product, Number(e.target.value))
                              )
                            }
                          >
                            {[...Array(item.countInStock).keys()].map((x) => (
                              <option key={x + 1} value={x + 1}>
                                {x + 1}
                              </option>
                            ))}
                          </FormControl>
                        </Col>
                        <Col md={2}>
                          <Button
                            type="button"
                            variant="light"
                            onClick={() => removeFromCartHandler(item.product)}
                          >
                            <i className="fas fa-trash"></i>
                          </Button>
                        </Col>
                      </Row>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              )}
            </Col>
            <Col md={4}>
              <Card>
                <ListGroup variant="flush">
                  {/* Calculation of All Product-Comment By Dulmina Herath */}
                  <ListGroup.Item>
                    <h2>
                      Subtotal of{" "}
                      {cartItems.reduce((acc, item) => acc + item.qty, 0)} Items
                    </h2>
                  </ListGroup.Item>
                  {/* Calculation of All Product to cost-Comment By Dulmina Herath */}
                  <ListGroup.Item>
                    Products Total: ${" "}
                    {cartItems
                      .reduce((acc, item) => acc + item.qty * item.price, 0)
                      .toFixed(2)}
                  </ListGroup.Item>
                  <ListGroup.Item>
                    + Tax (1.23%) : $
                    {/* Calculation of All Product to cost and tax-Comment By Dulmina Herath */}
                    {cartItems
                      .reduce(
                        (acc, item) =>
                          acc +
                          ((item.qty * item.price) / 100) * 1.23 +
                          item.qty * item.price,
                        0
                      )
                      .toFixed(2)}
                  </ListGroup.Item>
                  <ListGroup.Item>+ Shipping : $ 500.00</ListGroup.Item>
                  <ListGroup.Item>
                    <h2>
                      {" "}
                      {/* Calculation of All Product to cost,Shipping fee and tax-Comment By Dulmina Herath */}
                      Total Price : ${" "}
                      {(
                        parseFloat(
                          cartItems.reduce(
                            (acc, item) =>
                              acc +
                              (((item.qty * item.price) / 100) * 1.23 +
                                item.qty * item.price),
                            0
                          )
                        ) + 500.0
                      ).toFixed(2)}
                    </h2>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <Row>
                      <Link to="/paysuccess">
                        <Button
                          type="button"
                          className="btn-block"
                          disabled={cartItems.length === 0}
                          onClick={checkoutHandler}
                        >
                          Pay Now <i class="far fa-credit-card"></i>
                        </Button>
                      </Link>
                    </Row>
                  </ListGroup.Item>
                </ListGroup>
              </Card>
            </Col>
          </Row>
        </ListGroup.Item>
      </ListGroup>
    </React.Fragment>
  );
};

export default CartUi;
