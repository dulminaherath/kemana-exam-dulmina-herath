import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./maincomponents/Header";
import Footer from "./maincomponents/Footer";
import HomeScreen from "./interfaces/HomeScreen";
import ProductUi from "./interfaces/ProductUi";
import PaymentSuc from "./interfaces/PaymentSuc";
import CartUi from "./interfaces/CartUi";

function App() {
  return (
    <Router>
      <div>
        <Header />
        <main className="py-3">
          <Container>
            <Route path="/" component={HomeScreen} exact />
            <Route path="/product/:id" component={ProductUi} exact />
            <Route path="/cart/:id?" component={CartUi} exact />
            <Route path="/paysuccess" component={PaymentSuc} exact />
          </Container>
        </main>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
