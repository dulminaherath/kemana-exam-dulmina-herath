import React from 'react';
import { Col, Row } from 'react-bootstrap';
import products from '../productsbackend/products';
import Productcom from '../maincomponents/Productcom';

const HomeScreen = () => {
    return (
        <div>
            <h1>ALL Products</h1>
            <Row>
               {products.map(product=>(
                   <Col sm={12} md={6} lg={4} xl={3} key={product._id}>
                   <Productcom product={product}/>
                   </Col>
               ))}
            </Row>
        </div>
    )
}

export default HomeScreen


