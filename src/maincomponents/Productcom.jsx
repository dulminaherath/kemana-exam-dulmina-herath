import React from "react";
import { Card, Col, Row } from "react-bootstrap";
import Rating from "./Rating";
import { Link } from "react-router-dom";

//product view component-Comment By Dulmina Herath 
const Productcom = ({ history, product }) => {
  return (
    <div>
      <Card className="my-3 p-3 rounded">
        <Link to={`/product/${product._id}`}>
          <Card.Img src={product.image} variant="top" />
        </Link>
        <Card.Body>
          <Card.Title as="div">
            <strong>{product.name}</strong>
          </Card.Title>
          <Card.Text as="div">
            <Rating value={product.rating} text={` (${product.reviews})`} />
          </Card.Text>
          <Card.Text as="h3">
            <Row>
              <Col md={7}>$ {product.price}</Col>
              <Col md={2}>
                <Link to={`/cart/${product._id}?qty=1`}>
                  {" "}
                  {product.countInStock > 0 ? (
                    <i className="fas fa-shopping-cart"></i>
                  ) : (
                    ""
                  )}
                </Link>
              </Col>
              <Col md={1}>
                <Link to={`/product/${product._id}`}>
                  <i className="fas fa-eye"></i>
                </Link>
              </Col>
            </Row>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
};

export default Productcom;
