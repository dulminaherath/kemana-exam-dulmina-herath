import React from "react";
import { Spinner } from "react-bootstrap";

//i created this. but didnt used. because without the backend calling we didnt want this function-Comment By Dulmina Herath
const Loder = () => {
  return (
    <Spinner
      animation="border"
      role="status"
      style={{
        width: "100px",
        height: "100px",
        margin: "auto",
        display: "block",
      }}
    >
      <span className="sr-only">Loading...</span>
    </Spinner>
  );
};

export default Loder;
