import React from "react";
import { Alert } from "react-bootstrap";

//Message box-Comment By Dulmina Herath 
const Message = ({ variant, children }) => {
  return <Alert variant={variant}>{children}</Alert>;
};

Message.defaultProps = {
  variant: "info",
};

export default Message;
