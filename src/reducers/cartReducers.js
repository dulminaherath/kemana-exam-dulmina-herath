import {
  CART_ADD_ITEM,
  CART_REMOVE_ITEM,
  CART_ALLREMOVE_ITEM,
} from "../constants/cartConstants";

export const cartReducer = (state = { cartItems: [] }, action) => {
  switch (action.type) {
    //Get Cart Data-Comment By Dulmina Herath
    case CART_ADD_ITEM:
      const item = action.payload;

      const existItem = state.cartItems.find((x) => x.product === item.product);

      if (existItem) {
        return {
          ...state,
          cartItems: state.cartItems.map((x) =>
            x.product === existItem.product ? item : x
          ),
        };
      } else {
        return {
          ...state,
          cartItems: [...state.cartItems, item],
        };
      }

    //Remove Single Item from cart-Comment By Dulmina Herath
    case CART_REMOVE_ITEM:
      return {
        ...state,
        cartItems: state.cartItems.filter((x) => x.product !== action.payload),
      };

    //Remove All Items from cart-Comment By Dulmina Herath
    case CART_ALLREMOVE_ITEM:
      return {
        ...state,
        cartItems: [],
      };

    default:
      return state;
  }
};
